package edu.westga.cs6312.stock.controller;

import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Class to handle file input
 * 
 * @author JEANNIE BLAKELY
 * @version 4-20-18
 */
public class FileInteractor {
	
	/**
	 * Method that opens a file specified by the parameter, reads the data and uses 
	 * that data to populate a StockManager object
	 * 
	 * @param		filename 
	 * @return		theStockManager (StockManager object populated with data from file)
	 */
	public static StockManager readData(String filename) {
		File myFile = new File(filename);
		Scanner inFile = null;
		StockManager newStockManager = new StockManager();
		try {
			inFile = new Scanner(myFile);
			inFile.nextLine();
			while (inFile.hasNext()) {
				String input = inFile.nextLine();
				try {
					newStockManager.addStockRecord(FileInteractor.createStockRecordFromInput(filename, input));
				} catch (IllegalArgumentException iae) {
					System.out.println("There was a problem adding a StockRecord to the list");
				}
			}
			inFile.close();
		} catch (FileNotFoundException fnfe) {
			System.out.println("Data file does not exist");
			System.exit(0);
		} catch (NoSuchElementException nsee) {
			System.out.println("Read past the end of the file");
			
		} 
		return newStockManager;
	}
	
	private static StockRecord createStockRecordFromInput(String fileName, String input) {
		String[] stockInput = FileInteractor.splitString(input);
		if (stockInput.length == 7) {
			String date = stockInput[0].trim();
			double closingPrice = FileInteractor.getFileDouble(stockInput[4].trim());
			return FileInteractor.createStockRecord(fileName, date, closingPrice);
		} else {
			return null;
		}
	}
	
	private static StockRecord createStockRecord(String fileName, String date, double closingPrice) {
		StockRecord newStockRecord = null;
		try {
			newStockRecord = new StockRecord(fileName, date, closingPrice);
		} catch (IllegalArgumentException iae) {
			System.out.println("Invalid Data IllegalArgument");
		} catch (ParseException pe) {
			System.out.println("Invalid Data Parse");
		}
		return newStockRecord;
	}
	
	/**
	 * Splits a string into individual tokens
	 */
	private static String[] splitString(String stringToSplit) {
		String stockString = stringToSplit;
		String delimeter = "[,]";
		String[] stockTokens = stockString.split(delimeter);
		return stockTokens;
	}
	
	/**
	 * Converts string input from file into doubles
	 *@param 	input the file input
	 *@return 	double value
	 */
	private static double getFileDouble(String input) {
		double doubleValue = 0.0;
		try {
			doubleValue = Double.parseDouble(input);
		} catch (NumberFormatException ex) {
			System.out.println(input + " is not a valid number");
			doubleValue = -1.0;
		}
		return doubleValue;
	}
}
