package edu.westga.cs6312.stock.controller;

/**
 * This is a driver for the informal test application
 * @author JEANNIE BLAKELY
 * @version 4-20-18
 */
public class StockDriver {

	/**Entry point for the program. It creates and instance of VersionChooser
	 * and uses it to call chooseVersion
	 * @param args not used
	 */
	public static void main(String[] args) {
		VersionChooser theVersionChooser = new VersionChooser();
		theVersionChooser.chooseVersion();
	}
}
