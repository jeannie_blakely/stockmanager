package edu.westga.cs6312.stock.controller;

import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;
import edu.westga.cs6312.stock.view.StockPane;
import edu.westga.cs6312.stock.view.InteractivePane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Does the 5 things necessary for creating GUI applications
 * 
 * @author JEANNIE BLAKELY
 * @version 4-23-18
 *
 */
public class StockGUI extends Application {
	private static StockManager theStockManager;
	
	/**
	 * Creates static StockManager object from object created from file
	 * 
	 * @param 	myStockManager	StockManager object created from file
	 */
	public static void setManager(StockManager myStockManager) {
		StockGUI.theStockManager = new StockManager();
		for (StockRecord current : myStockManager.getAllRecords()) {
			StockGUI.theStockManager.addStockRecord(current);
		}
	}
	
	/**Entry point for the program. It creates a StockManager object and stores it and
	 * creates a StockPane object and adds it to the Scene.
	 * @param 	primaryStage object
	 */
	@Override
	public void start(Stage primaryStage) {
		StockPane myStockPane = new StockPane(StockGUI.theStockManager);
		InteractivePane myInteractivePane = new InteractivePane(myStockPane);
		Scene scene = new Scene(myInteractivePane);
		//Scene scene = new Scene(myStockPane);
		primaryStage.setTitle("CS6312 Final: Bulls and Bears");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
