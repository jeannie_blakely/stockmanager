package edu.westga.cs6312.stock.controller;

import edu.westga.cs6312.stock.model.StockManager;

import edu.westga.cs6312.stock.view.StockTUI;

import java.util.Scanner;

/**
 * Prompts user for filename and calls handlers
 * 
 * @author JEANNIE BLAKELY
 * @version 4-20-18
 */
public class VersionChooser {
	private Scanner scan;
	private StockManager theStockManager;
	
	/**
	 * 0 Parameter constructor used to set up the Scanner instance variable
	 */
	public VersionChooser() {
		this.scan = new Scanner(System.in);
		this.theStockManager = new StockManager();
	}
	
	/**
	 * Calls getFileName, FileInteractor.readData, and runTextOption
	 */
	public void chooseVersion() {
		this.getDisplay("Welcome to the StockManager Application");
		this.theStockManager = FileInteractor.readData(this.getFileName());
		if (this.theStockManager != null) {
			this.runChooseVersion();
		}
	}
	
	/**
	 * Uses the console to prompt the user for a file to be read
	 * 
	 * @return 			filename
	 */
	private String getFileName() {
		String fileName = "";
		this.getDisplayNoBreak("Please enter a filename: ");
		fileName = this.scan.nextLine();
		return fileName;
	}
	
	/**
	 * Converts string input into System.out.println display
	 */
	private void getDisplay(String message) {
		System.out.println(message);
	}
	
	/**
	 * Converts string input into System.out.print display
	 */
	private void getDisplayNoBreak(String message) {
		System.out.print(message);
	}
	
	/**
	 * Allows user to choose between text or graphical version
	 */
	private void runChooseVersion() {
		this.getDisplay("");
		int choice = 0;
		do {
			this.displayMenu();
			choice = this.getUserInt("Please enter your choice: ");
			switch (choice) {
				case 1:
					this.getDisplay("");
					this.runTextOption();
					break;
				case 2:
					this.getDisplay("");
					this.runGraphicalOption();
					break;
				case 3:
					break;
				default:
					this.getDisplay("That was not a valid selection. Please enter an integer from 1 - 4.");
					this.getDisplay("");
					break;
			} System.out.println();
		} while (choice != 3);
	}
	
	/**
	 * Displays a list of menu items to the console.
	 */
	private void displayMenu() {
		System.out.println("\t 1 - Run Text Version");
		System.out.println("\t 2 - Run Graphical Version");
		System.out.println("\t 3 - Quit");
	}
	
	/**
	 * Converts string input into System.out.println display
	 */
	
	/**
	 * Converts string input into integers
	 *@param 	message instruction to be printed to the user
	 *@return 	int value
	 */
	private int getUserInt(String message) {
		int intInput = 0;
		String input = "";
		boolean isValid = false;
		while (!isValid) {
			try {
				System.out.print(message);
				input = this.scan.nextLine();
				intInput = Integer.parseInt(input);
				isValid = true;
			} catch (NumberFormatException ex) {
				System.out.println(input + " is not a valid entry.  Please enter an integer");
			} 
		} 
		return intInput;
	}
	
	protected void runGraphicalOption() {
		StockGUI.setManager(this.theStockManager);
		StockGUI.launch(StockGUI.class);
	}
	
	private void runTextOption() {
		StockTUI myStockTUI = new StockTUI(this.theStockManager);
		myStockTUI.run();
	}
}
