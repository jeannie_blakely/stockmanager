package edu.westga.cs6312.stock.model;

import java.util.ArrayList;


/**
 * Manages a list of StockRecord objects.
 * 
 * @author JEANNIE BLAKELY
 * @version 4-20-18
 */
public class StockManager {
	private ArrayList<StockRecord> theStockRecords;
	
	/**
	 * 0 parameter constructor to set up the StockManager object.
	 */
	public StockManager() {
		this.theStockRecords = new ArrayList<StockRecord>();
	}
	
	/**
	 * Accepts StockRecord object to be added to ArrayList
	 * @precondition		myStockRecord != null
	 * @param				myStockRecord object
	 */
	public void addStockRecord(StockRecord myStockRecord) {
		if (myStockRecord == null) {
			throw new IllegalArgumentException("Invalid StockRecord");
		}
		this.theStockRecords.add(myStockRecord);
	}
	
	/**
	 * Returns the filename (used to get the stock symbol)
	 * @return 		fileName
	 */
	public String getFileName() {
		return this.getStockRecord(0).getFileName();
	}
	
	/**
	 * Returns an ArrayList<StockRecord>
	 * @return				this.theStockRecords
	 */
	public ArrayList<StockRecord> getAllRecords() {
		return this.theStockRecords;
	}
	
	/**
	 * Returns ArrayList size
	 * @return				this.theStockRecords.size
	 */
	public int getStockManagerSize() {
		return this.theStockRecords.size();
	}

	/**
	 * Returns a StockRecord at a given index
	 * @precondition		index >=0 && <= theStockRecords.size - 1
	 * @param				index 
	 * @return				StockRecord at index
	 */
	public StockRecord getStockRecord(int index) {
		if (index < 0 || index > this.theStockRecords.size() - 1) {
			throw new IllegalArgumentException("Invalid ArrayList index");
		}
		return this.theStockRecords.get(index);
	}
	
	/**
	 * Returns lowest closingPrice on StockManager list
	 * @return				lowestClosingPrice
	 */
	public double getLowestClosingPrice() {
		return java.util.Collections.min(this.theStockRecords).getClosingPrice();
	}
	
	/**
	 * Returns highest closingPrice on StockManager list
	 * @return				highestClosingPrice
	 */
	public double getHighestClosingPrice() {
		return java.util.Collections.max(this.theStockRecords).getClosingPrice();
	}
	
	/**
	 * Gets the average closing price of StockRecords
	 * 
	 * @return 				totalClosing / number of StockRecords
	 */
	public double getAverageClosingPrice() {
		double totalClosing = 0.0;
		for (StockRecord current : this.theStockRecords) {
			totalClosing = totalClosing + current.getClosingPrice();
		}
		return totalClosing / this.theStockRecords.size();
	}
	
	/**
	 * Returns a list of StockRecord objects one per line
	 * @return				String list of StockRecord
	 */
	public String toString() {
		String stockRecordList = "";
		for (StockRecord current : this.theStockRecords) {
			stockRecordList += current.toString() + "\n";
		}
		return stockRecordList;
	}

}
