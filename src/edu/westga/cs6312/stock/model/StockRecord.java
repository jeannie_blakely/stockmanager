package edu.westga.cs6312.stock.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A class for StockRecord objects.  It keeps track of information about 
 * a particular stock on a given day
 * 
 * @author JEANNIE BLAKELY
 * @version 4-18-18
 */
public class StockRecord implements Comparable<StockRecord> {
	private String fileName;
	private Date historicalDate;
	private double closingPrice;
	
	/**
	 * 2 parameter constructor that creates a new instance of StockRecord 
	 * accepts the date, the closing price and initializes the variables
	 * 
	 * @param			date String date			
	 * @param 			closingPrice double value
	 * @param			fileName String input filename
	 * @throws 			ParseException ns
	 * @precondition 	name != null
	 * @precondition	date != null
	 */
	public StockRecord(String fileName, String date, double closingPrice) throws ParseException {
		if (fileName == null) {
			throw new IllegalArgumentException("Invalid filename; StockRecord not created");
		}
		if (date == null) {
			throw new IllegalArgumentException("Invalid Date; StockRecord not created");
		}
		if (closingPrice < 0) {
			throw new IllegalArgumentException("Invalid Closing Price; StockRecord not created");
		}
		this.fileName = fileName;
		this.closingPrice = closingPrice;
		this.historicalDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
	}
	
	/**
	 * Returns the filename (used to get the stock symbol)
	 * @return 		fileName
	 */
	public String getFileName() {
		return this.fileName;
	}
	
	/**
	 * Returns the closingPrice
	 * @return 		closingPrice
	 */
	public double getClosingPrice() {
		return this.closingPrice;
	}
	
	/**
	 * Returns the historicalDate
	 * @return 		historicalDate
	 */
	public Date getHistoricalDate() {
		return this.historicalDate;
	}
	
	/**
	 * Returns the historicalDate formatted as yyyy-MM-dd
	 * @param		dateFormat formatted string for Date
	 * @return 		Date object formatted into String
	 */
	public String formatHistoricalDate(String dateFormat) {
		SimpleDateFormat formattedDate = new SimpleDateFormat(dateFormat);
		return formattedDate.format(this.historicalDate);
	}
	
	/**
	 * Returns a String representation of StockRecord object
	 * @return 		String representation of StockRecord object
	 */
	public String toString() {
		return "Closed at $" + String.format("%.02f", this.closingPrice) + " on " + this.formatHistoricalDate("yyyy-MM-dd");
	}
	
	/**
	 * Compares closingPrice of StockRecord objects.  To 6 decimal place accuracy
	 * @return 		int neg, 0, pos for closingPrice less than, equal to or greater than
	 */
	@Override
	public int compareTo(StockRecord otherStockRecord) {
		if (otherStockRecord == null) {
			throw new NullPointerException();
		}
		if (!(otherStockRecord instanceof StockRecord)) {
			throw new ClassCastException();
		}
		return (int) (1000000 * (this.closingPrice - otherStockRecord.getClosingPrice()));
	}
}
