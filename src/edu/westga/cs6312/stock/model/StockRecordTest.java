package edu.westga.cs6312.stock.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

class StockRecordTest {
	
	/**
	 * Test to confirm StockRecord is created and it's toString
	 * works
	 */
	@Test
	public void testToStringShouldBeClosedat23dot34on20180319() throws ParseException {
		StockRecord myStockRecord = new StockRecord("3/19/2018", 23.3495);
		assertEquals("Closed at 23.35 on 2018-03-19", myStockRecord.toString());
	}
	
	/**
	 * Another test to confirm StockRecord is created and it's toString
	 * works
	 */
	public void testToStringShouldBeClosedat23dot03on20180410() throws ParseException {
		StockRecord myStockRecord = new StockRecord("4/10/2018", 23.0301);
		assertEquals("Closed at 23.03 on 2018-04-10", myStockRecord.toString());
		
	}

}
