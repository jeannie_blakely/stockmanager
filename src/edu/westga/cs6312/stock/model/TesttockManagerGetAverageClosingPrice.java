package edu.westga.cs6312.stock.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

class TesttockManagerGetAverageClosingPrice {

	/**
	 * Test to confirm the getAverageClosingPrice method works on one StockRecord
	 * ; should be 23.32 
	 */
	@Test
	public void testStockManagerAddShouldBe23dot32() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("3/21/2018", 23.32);
			newStockManager.addStockRecord(newStockRecord);
			assertEquals(23.32, newStockManager.getAverageClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getAverageClosingPrice method works on 3 StockRecords
	 * ; should be 
	 */
	@Test
	public void testStockManagerAddShouldBe23dot12673etc() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("3/21/2018", 23.32);
			StockRecord anotherStockRecord = new StockRecord("4/10/2018", 23.0301);
			StockRecord additionalStockRecord = new StockRecord("3/19/2018", 23.0301);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(anotherStockRecord);
			newStockManager.addStockRecord(additionalStockRecord);
			assertEquals(23.126733333333334, newStockManager.getAverageClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}

}
