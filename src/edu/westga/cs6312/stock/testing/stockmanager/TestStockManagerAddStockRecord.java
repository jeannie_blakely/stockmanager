package edu.westga.cs6312.stock.testing.stockmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;

class TestStockManagerAddStockRecord {
	
	/**
	 * Test to confirm the addStockRecord method works
	 */
	@Test
	public void testStockManagerAddShouldBe23dot32on20180321() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			newStockManager.addStockRecord(newStockRecord);
			assertEquals("Closed at $23.32 on 2018-03-21\n", newStockManager.toString());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the addStockRecord method works
	 */
	@Test
	public void testStockManagerAddShouldBe23dot03on20180410() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			newStockManager.addStockRecord(newStockRecord);
			assertEquals("Closed at $23.03 on 2018-04-10\n", newStockManager.toString());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}

}
