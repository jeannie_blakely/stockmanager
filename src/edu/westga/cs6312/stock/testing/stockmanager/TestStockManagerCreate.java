package edu.westga.cs6312.stock.testing.stockmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockManager;

class TestStockManagerCreate {
	
	/**
	 * Test to confirm StockManager is created and its toString
	 * works
	 */
	@Test
	public void testStockManagerShouldBeEmptyString() {
		StockManager newStockManager = new StockManager();
		assertEquals("", newStockManager.toString());
	}

}
