package edu.westga.cs6312.stock.testing.stockmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;

class TestStockManagerGetAverageClosingPrice {

	/**
	 * Test to confirm the getAverageClosingPrice method works on one StockRecord
	 * ; should be 23.32 
	 */
	@Test
	public void testStockManagerAddShouldBe23dot32() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			newStockManager.addStockRecord(newStockRecord);
			assertEquals(23.32, newStockManager.getAverageClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getAverageClosingPrice method works on 3 StockRecords
	 * ; should be 
	 */
	@Test
	public void testStockManagerAddShouldBe23dot12673etc() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			StockRecord anotherStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			StockRecord additionalStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.0301);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(anotherStockRecord);
			newStockManager.addStockRecord(additionalStockRecord);
			assertEquals(23.126733333333334, newStockManager.getAverageClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getAverageClosingPrice method works on one StockRecord
	 * ; should be 23.32 
	 */
	@Test
	public void testStockManagerAddShouldBe23dot094() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-4-12", 23.139999);
			StockRecord newStockRecord2 = new StockRecord("BML-PL.csv", "2018-4-13", 23.049999);
			StockRecord newStockRecord3 = new StockRecord("BML-PL.csv", "2018-4-16", 23.08);
			StockRecord newStockRecord4 = new StockRecord("BML-PL.csv", "2018-4-17", 23.139999);
			StockRecord newStockRecord5 = new StockRecord("BML-PL.csv", "2018-4-18", 23.059999);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(newStockRecord2);
			newStockManager.addStockRecord(newStockRecord3);
			newStockManager.addStockRecord(newStockRecord4);
			newStockManager.addStockRecord(newStockRecord5);
			assertEquals(23.093999200000003, newStockManager.getAverageClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}

}
