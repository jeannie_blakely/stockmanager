package edu.westga.cs6312.stock.testing.stockmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;

class TestStockManagerGetFileName {

	/**
	 * Test to confirm the getFileName method works on one StockRecord
	 * ; should be BML-PL.csv 
	 */
	@Test
	public void testStockManagerGetFileNameShouldBeBMLdashPLdotcsv() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			newStockManager.addStockRecord(newStockRecord);
			assertEquals("BML-PL.csv", newStockManager.getFileName());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getFileName method works on one StockRecord
	 * ; should be BML-PL.csv 
	 */
	@Test
	public void testStockManagerGetFileNameShouldBeAAPLdotcsv() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("AAPL.csv", "2018-3-21", 23.32);
			newStockManager.addStockRecord(newStockRecord);
			assertEquals("AAPL.csv", newStockManager.getFileName());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
}
