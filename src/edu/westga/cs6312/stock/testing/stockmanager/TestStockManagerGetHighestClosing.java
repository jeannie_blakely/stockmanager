package edu.westga.cs6312.stock.testing.stockmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;
import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;

class TestStockManagerGetHighestClosing {

	/**
	 * Test to confirm the getHighestClosingPrice method works on 5 StockRecord
	 * ; should be 23.049999 
	 */
	@Test
	public void testStockManagerHighestClosingPriceShouldBe23dot139999() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-4-12", 23.139999);
			StockRecord newStockRecord2 = new StockRecord("BML-PL.csv", "2018-4-13", 23.049999);
			StockRecord newStockRecord3 = new StockRecord("BML-PL.csv", "2018-4-16", 23.08);
			StockRecord newStockRecord4 = new StockRecord("BML-PL.csv", "2018-4-17", 23.139999);
			StockRecord newStockRecord5 = new StockRecord("BML-PL.csv", "2018-4-18", 23.059999);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(newStockRecord2);
			newStockManager.addStockRecord(newStockRecord3);
			newStockManager.addStockRecord(newStockRecord4);
			newStockManager.addStockRecord(newStockRecord5);
			assertEquals(23.139999, newStockManager.getHighestClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getLowestClosingPrice  method works on 3 StockRecords
	 * ; should be 23.0301
	 */
	@Test
	public void testStockManagerHighestClosingPriceShouldBe23dot32() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			StockRecord anotherStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			StockRecord additionalStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.0301);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(anotherStockRecord);
			newStockManager.addStockRecord(additionalStockRecord);
			assertEquals(23.32, newStockManager.getHighestClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getLowestClosingPrice method works on 2 StockRecord
	 * ; should be 23.049999 
	 */
	@Test
	public void testStockManagerHighestClosingPriceShouldBe23dot35() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-4-12", 23.35);
			StockRecord newStockRecord3 = new StockRecord("BML-PL.csv", "2018-4-16", 23.08);
			StockRecord newStockRecord5 = new StockRecord("BML-PL.csv", "2018-4-18", 23.059999);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(newStockRecord3);
			newStockManager.addStockRecord(newStockRecord5);
			assertEquals(23.35, newStockManager.getHighestClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
}
