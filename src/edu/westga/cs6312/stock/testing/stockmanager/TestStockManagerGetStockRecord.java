package edu.westga.cs6312.stock.testing.stockmanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;

class TestStockManagerGetStockRecord {

	/**
	 * Test to confirm the getStockRecord method works at 0 index
	 */
	@Test
	public void testStockManagerGetStockRecordShouldBe23dot32on20180321() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			StockRecord anotherStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			StockRecord additionalStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.0301);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(anotherStockRecord);
			newStockManager.addStockRecord(additionalStockRecord);
			assertEquals("Closed at $23.32 on 2018-03-21", newStockManager.getStockRecord(0).toString());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getStockRecord method works at 2 index
	 */
	@Test
	public void testStockManagerGetStockRecordShouldBe23dot03on20180319() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			StockRecord anotherStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			StockRecord additionalStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.0301);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(anotherStockRecord);
			newStockManager.addStockRecord(additionalStockRecord);
			assertEquals("Closed at $23.03 on 2018-03-19", newStockManager.getStockRecord(2).toString());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm the getStockRecord method works at 4 index
	 */
	@Test
	public void testStockManagerGetStockRecordShouldBe23dot06on20180418() {
		try {
			StockManager newStockManager = new StockManager();
			StockRecord newStockRecord = new StockRecord("BML-PL.csv", "2018-4-12", 23.139999);
			StockRecord newStockRecord2 = new StockRecord("BML-PL.csv", "2018-4-13", 23.049999);
			StockRecord newStockRecord3 = new StockRecord("BML-PL.csv", "2018-4-16", 23.08);
			StockRecord newStockRecord4 = new StockRecord("BML-PL.csv", "2018-4-17", 23.139999);
			StockRecord newStockRecord5 = new StockRecord("BML-PL.csv", "2018-4-18", 23.059999);
			newStockManager.addStockRecord(newStockRecord);
			newStockManager.addStockRecord(newStockRecord2);
			newStockManager.addStockRecord(newStockRecord3);
			newStockManager.addStockRecord(newStockRecord4);
			newStockManager.addStockRecord(newStockRecord5);
			assertEquals("Closed at $23.06 on 2018-04-18", newStockManager.getStockRecord(4).toString());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}

}
