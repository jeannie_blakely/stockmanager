package edu.westga.cs6312.stock.testing.stockrecord;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockRecord;

class TestStockRecordCompareTo {
	
	/**
	 * Test to see if CompareTo override method returns neg when 1st StockRecord's
	 * closingPrice is less than the one it's compared to.
	 */
	@Test
	public void testCompareToShouldBeNeg31939() {
		try {
			StockRecord betterStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.3495);
			StockRecord worseStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			assertEquals(-319399, worseStockRecord.compareTo(betterStockRecord));
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to see if CompareTo override method returns pos when 1st StockRecord's
	 * closingPrice is greater than the one it's compared to.
	 */
	@Test
	public void testCompareToShouldBePos9989() {
		try {
			StockRecord betterStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.129999);
			StockRecord worseStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			assertEquals(99899, betterStockRecord.compareTo(worseStockRecord));
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to see if CompareTo override method returns 0 when 1st StockRecord's
	 * closingPrice is equal to the one it's compared to.
	 */
	@Test
	public void testCompareToShouldBe0() {
		try {
			StockRecord theStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.0301);
			StockRecord theOtherStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			assertEquals(0, theStockRecord.compareTo(theOtherStockRecord));
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
}
