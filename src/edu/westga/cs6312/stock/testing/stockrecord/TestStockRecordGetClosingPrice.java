package edu.westga.cs6312.stock.testing.stockrecord;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockRecord;

class TestStockRecordGetClosingPrice {

	/**
	 * Test to confirm StockRecord is created and getClosingPrice is
	 * printing out correct result
	 */
	@Test
	public void testGetClosingPriceShouldBe23dot32() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			assertEquals(23.32, myStockRecord.getClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm StockRecord is created and getClosingPrice is
	 * printing out correct result
	 */
	@Test
	public void testGetClosingPriceShouldBe23dot0301() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			assertEquals(23.0301, myStockRecord.getClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm StockRecord is created and getClosingPrice is
	 * printing out correct result
	 */
	@Test
	public void testGetClosingPriceShouldBe23dot5() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.5);
			assertEquals(23.5, myStockRecord.getClosingPrice());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
}
