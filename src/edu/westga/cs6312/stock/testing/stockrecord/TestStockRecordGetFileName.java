package edu.westga.cs6312.stock.testing.stockrecord;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockRecord;

class TestStockRecordGetFileName {

	/**
	 * Test to confirm StockRecord is created and getFileName method is
	 * returning correct result
	 */
	@Test
	public void testGetFileNameShouldBeBMLdashPLdotcsv() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			assertEquals("BML-PL.csv", myStockRecord.getFileName());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm StockRecord is created and getFileName method is
	 * returning correct result
	 */
	@Test
	public void testGetFileNameShouldBeAAPLdotcsv() {
		try {
			StockRecord myStockRecord = new StockRecord("AAPL.csv", "2018-3-21", 23.32);
			assertEquals("AAPL.csv", myStockRecord.getFileName());
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
}
