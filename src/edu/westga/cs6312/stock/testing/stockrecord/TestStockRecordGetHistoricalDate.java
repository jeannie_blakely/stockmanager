package edu.westga.cs6312.stock.testing.stockrecord;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockRecord;

class TestStockRecordGetHistoricalDate {
	
	/**
	 * Test to confirm StockRecord is created and the getHistoricalDate is returning the correct date
	 */
	@Test
	public void testGetHistoricalDateShouldBeMar192018() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.3495);
			assertEquals("Mon Mar 19 00:00:00 EDT 2018", myStockRecord.getHistoricalDate().toString());
		}  catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}

	/**
	 * Test to confirm StockRecord is created and the getHistoricalDate is returning the correct date
	 */
	@Test
	public void testGetHistoricalDateShouldBeApr102018() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			assertEquals("Tue Apr 10 00:00:00 EDT 2018", myStockRecord.getHistoricalDate().toString());
		}  catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm StockRecord is created and the getHistoricalDate is returning the correct date
	 */
	@Test
	public void testGetHistoricalDateShouldBeMar232018() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-23", 23.5);
			assertEquals("Fri Mar 23 00:00:00 EDT 2018", myStockRecord.getHistoricalDate().toString());
		}  catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
}
