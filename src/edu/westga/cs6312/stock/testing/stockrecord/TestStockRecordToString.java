package edu.westga.cs6312.stock.testing.stockrecord;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.stock.model.StockRecord;

class TestStockRecordToString {

	/**
	 * Test to confirm StockRecord is created and it's toString
	 * works
	 */
	@Test
	public void testToStringShouldBeClosedat23dot34on20180319() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-19", 23.3495);
			assertEquals("Closed at $23.35 on 2018-03-19", myStockRecord.toString());
		}  catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Another test to confirm StockRecord is created and it's toString
	 * works
	 */
	@Test
	public void testToStringShouldBeClosedat23dot03on20180410() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			assertEquals("Closed at $23.03 on 2018-04-10", myStockRecord.toString());
		}  catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Another test to confirm StockRecord is created and it's toString
	 * works (checks formatting of closingPrice)
	 */
	@Test
	public void testToStringShouldBeClosedat23dot50on20180323() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-23", 23.5);
			assertEquals("Closed at $23.50 on 2018-03-23", myStockRecord.toString());
		}  catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
}
