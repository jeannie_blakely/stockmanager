package edu.westga.cs6312.stock.testing.stockrecord;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.westga.cs6312.stock.model.StockRecord;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

class TestStockRecordformatHistoricalDate {

	/**
	 * Test to confirm StockRecord is created and formatHistoricalDate is
	 * printing out correct result
	 */
	@Test
	public void testFormatHistoricalDateShouldBe3dash21dash2018() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.32);
			assertEquals("03-21-2018", myStockRecord.formatHistoricalDate("MM-dd-yyyy"));
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm StockRecord is created and formatHistoricalDate is
	 * printing out correct result
	 */
	@Test
	public void testFormatHistoricalDateShouldBe2018slash4slash10() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-4-10", 23.0301);
			assertEquals("2018/10/4", myStockRecord.formatHistoricalDate("yyyy/d/M"));
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}
	
	/**
	 * Test to confirm StockRecord is created and formatHistoricalDate is
	 * printing out correct result
	 */
	@Test
	public void testFormatHistoricalDateShouldBe3slash21slash18() {
		try {
			StockRecord myStockRecord = new StockRecord("BML-PL.csv", "2018-3-21", 23.5);
			assertEquals("3/21/18", myStockRecord.formatHistoricalDate("M/d/yy"));
		} catch (ParseException pe) {
			System.out.println("Invalid date");
		}
	}

}
