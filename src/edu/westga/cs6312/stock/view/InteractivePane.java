package edu.westga.cs6312.stock.view;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import edu.westga.cs6312.stock.controller.FileInteractor;
import edu.westga.cs6312.stock.model.StockManager;

/**
 * Creates class InteractivePane which is a special kind of GridPane.  Contains
 * code to create the GUI interface to choose a file and display the resulting StockPane
 * 
 * @author JEANNIE BLAKELY
 * @version 4-30-18
 */
public class InteractivePane extends GridPane {
	private StockPane theStockPane;
	private String fileName;
	
	/**
	 * 1 Parameter constructor that accepts a StockPane object to be stored in the
	 * instance variable.  Also contains code to build the interface
	 * 
	 * @param				myStockPane StockPane object
	 * @preconditions		myStockPane != null
	 */
	public InteractivePane(StockPane myStockPane) {
		this.fileName = "";
		this.theStockPane = myStockPane;
		this.setPrefSize(700, 550);
		this.drawInteractivePane();
	}
	
	/**
	 * Calls helper methods and uses StockPane to draw the InteractivePane
	 */
	private void drawInteractivePane() {
		this.getChildren().clear();
		this.setAlignment(Pos.CENTER);
		this.setHgap(5);
		this.setVgap(5);
		this.drawStockPane();
		this.drawFileChooserMenu();
	}
	
	/**
	 * Sets up StockPane on InteractivePane
	 */
	private void drawStockPane() {
		this.getChildren().clear();
		InteractivePane.setConstraints(this.theStockPane, 1, 2);
		InteractivePane.setHgrow(this.theStockPane, Priority.ALWAYS);
		InteractivePane.setVgrow(this.theStockPane, Priority.ALWAYS);
		this.getChildren().add(this.theStockPane);
	}
	
	/**
	 * Draws file chooser button and file label and adds them to a HBox
	 */
	private void drawFileChooserMenu() {
		Button fileButton = new Button("Choose File");
		Label fileLabel = new Label("File: " + this.fileName);
		HBox hBoxFileChooser = new HBox();
		hBoxFileChooser.setSpacing(10);
		hBoxFileChooser.getChildren().addAll(fileButton, fileLabel);
		hBoxFileChooser.setAlignment(Pos.CENTER);
		InteractivePane.setConstraints(hBoxFileChooser, 1,  1);
		this.getChildren().add(hBoxFileChooser);
		fileButton.setOnAction(new FileChooserHandler());
	}
	
	/**
	 * Handler that sets up FileChooser object and calls method to run the 
	 * file and create the StockManager object when file is selected
	 */
	private class FileChooserHandler implements EventHandler<ActionEvent> {
		public void handle(final ActionEvent event) {
			Stage fileStage = new Stage();
			FileChooser stockFileChooser = new FileChooser();
			InteractivePane.configureFileChooser(stockFileChooser);
			File stockFile = stockFileChooser.showOpenDialog(fileStage);
			if (stockFile != null) {
				InteractivePane.this.runFile(stockFile);
			}
		}
	}
	
	/**
	 * Uses file chosen and calls FileInteractor to create new StockManager object 
	 * and redraws InteractivePane with new StockManager data 
	 */
	private void runFile(File myFile) {
		StockManager newStockManager = new StockManager();
		newStockManager = FileInteractor.readData(myFile.getName());
		this.theStockPane = new StockPane(newStockManager);
		this.fileName = myFile.getName();
		this.drawInteractivePane();
	}
	
	/**
	 * Method to specify the Eclipse working directory for FileChooser object
	 */
	private static void configureFileChooser(final FileChooser myFileChooser) {
		myFileChooser.setTitle("Choose Stock Data File");
		myFileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
	}
}
