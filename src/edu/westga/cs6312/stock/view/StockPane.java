package edu.westga.cs6312.stock.view;

import edu.westga.cs6312.stock.model.StockManager;
import edu.westga.cs6312.stock.model.StockRecord;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Creates class StockPane which is a special kind of Pane.  Contains
 * code to create the GUI interface for StockManager
 * 
 * @author JEANNIE BLAKELY
 * @version 4-28-18
 */
public class StockPane extends Pane {
	private StockManager theStockManager;

	/**
	 * 1 Parameter constructor that accepts a StockManager object to be stored in the
	 * instance variable.  Sets up appropriately sized Pane and has the stock data
	 * drawn
	 * 
	 * @param				myStockManager StockManager object (passed from VersionChooser)
	 * @preconditions		theStockManager != null
	 */
	public StockPane(StockManager myStockManager) {
		this.theStockManager = myStockManager;
		this.setPrefSize(700, 450);
		this.drawStockPane();
	}

	/**
	 * Calls helper methods to draw the StockPane
	 */
	private void drawStockPane() {
		this.drawBottomTimeLine();
		this.drawLowestPrice();
		this.drawTopCenterTimeLine();
		this.drawTopCenterPrice();
		this.drawHighestPrice();
		this.drawCenterTimeLine();
		this.drawMiddlePrice();
		this.drawBottomCenterTimeLine();
		this.drawBottomCenterPrice();
		this.drawTopTimeLine();
		this.drawAveragePriceLine();
		this.drawFirstDate();
		this.drawLastDate();
		this.drawMiddleDate();
		this.drawFirstPriceLine();
		this.drawLastPriceLine();
		this.drawMiddlePriceLine();
		this.drawStockSymbol();
		this.drawStockPoints();
	}
	
	/**
	 * Draws bottom horizontal time line
	 */
	private void drawBottomTimeLine() {
		Line timeLine = new Line();
		timeLine.setStartX(50);
		timeLine.startYProperty().bind(heightProperty().subtract(50));
		timeLine.endXProperty().bind(widthProperty().subtract(50));
		timeLine.endYProperty().bind(heightProperty().subtract(50));
		timeLine.setStrokeWidth(1);
		timeLine.setStroke(Color.BLACK);
		getChildren().add(timeLine);
	}
	
	/**
	 * Draws lowest price on bottom timeline
	 */
	private void drawLowestPrice() {
		Text lowestPrice = new Text();
		lowestPrice.setText("$" +  String.format("%.02f", this.theStockManager.getLowestClosingPrice()));
		lowestPrice.setX(10);
		lowestPrice.yProperty().bind(heightProperty().subtract(50));
		lowestPrice.setFont(Font.font("Courier", 12));
		getChildren().add(lowestPrice);
	}
	
	/**
	 * Draws top horizontal time line
	 */
	private void drawTopTimeLine() {
		Line timeLine = new Line();
		timeLine.setStartX(50);
		timeLine.setStartY(50);
		timeLine.endXProperty().bind(widthProperty().subtract(50));
		timeLine.setEndY(50);
		timeLine.setStrokeWidth(1);
		timeLine.setStroke(Color.BLACK);
		getChildren().add(timeLine);
	}
	
	/**
	 * Draws highest price on top timeline
	 */
	private void drawHighestPrice() {
		Text highestPrice = new Text();
		highestPrice.setText("$" + String.format("%.02f", this.theStockManager.getHighestClosingPrice()));
		highestPrice.setX(10);
		highestPrice.setY(50);
		highestPrice.setFont(Font.font("Courier", 12));
		getChildren().add(highestPrice);
	}
	
	/**
	 * Draws center horizontal time line
	 */
	private void drawCenterTimeLine() {
		Line timeLine = new Line();
		timeLine.setStartX(50);
		timeLine.startYProperty().bind(heightProperty().multiply(0.5));
		timeLine.endXProperty().bind(widthProperty().subtract(50));
		timeLine.endYProperty().bind(heightProperty().multiply(0.5));
		timeLine.setStrokeWidth(1);
		timeLine.setStroke(Color.BLACK);
		getChildren().add(timeLine);
	}
	
	/**
	 * Draws middle price on top timeline
	 */
	private void drawMiddlePrice() {
		Text middlePrice = new Text();
		middlePrice.setText("$" + String.format("%.02f", ((this.theStockManager.getHighestClosingPrice()) + this.theStockManager.getLowestClosingPrice()) / 2));
		middlePrice.setX(10);
		middlePrice.yProperty().bind(heightProperty().multiply(0.5));
		middlePrice.setFont(Font.font("Courier", 12));
		getChildren().add(middlePrice);
	}
	
	/**
	 * Draws top center horizontal time line
	 */
	private void drawTopCenterTimeLine() {
		Line timeLine = new Line();
		timeLine.setStartX(50);
		timeLine.startYProperty().bind(heightProperty().subtract(100).multiply(0.25).add(50));
		timeLine.endXProperty().bind(widthProperty().subtract(50));
		timeLine.endYProperty().bind(heightProperty().subtract(100).multiply(0.25).add(50));
		timeLine.setStrokeWidth(1);
		timeLine.setStroke(Color.BLACK);
		getChildren().add(timeLine);
	}
	

	/**
	 * Draws top middle price on top center timeline
	 */
	private void drawTopCenterPrice() {
		Text topCenterPrice = new Text();
		double middleValue = (this.theStockManager.getHighestClosingPrice() + this.theStockManager.getLowestClosingPrice()) / 2;
		topCenterPrice.setText("$" + String.format("%.02f", (middleValue + this.theStockManager.getHighestClosingPrice()) / 2));
		topCenterPrice.setX(10);
		topCenterPrice.yProperty().bind(heightProperty().subtract(100).multiply(0.25).add(50));
		topCenterPrice.setFont(Font.font("Courier", 12));
		getChildren().add(topCenterPrice);
	}
	
	/**
	 * Draws center horizontal time line
	 */
	private void drawBottomCenterTimeLine() {
		Line timeLine = new Line();
		timeLine.setStartX(50);
		timeLine.startYProperty().bind(heightProperty().subtract(100).multiply(0.75).add(50));
		timeLine.setEndX(650);
		timeLine.endXProperty().bind(widthProperty().subtract(50));
		timeLine.endYProperty().bind(heightProperty().subtract(100).multiply(0.75).add(50));
		timeLine.setStrokeWidth(1);
		timeLine.setStroke(Color.BLACK);
		getChildren().add(timeLine);
	}
	
	/**
	 * Draws bottom middle price on bottom center timeline
	 */
	private void drawBottomCenterPrice() {
		Text bottomCenterPrice = new Text();
		double middleValue = (this.theStockManager.getHighestClosingPrice() + this.theStockManager.getLowestClosingPrice()) / 2;
		bottomCenterPrice.setText("$" + String.format("%.02f", (middleValue + this.theStockManager.getLowestClosingPrice()) / 2));
		bottomCenterPrice.setX(10);
		bottomCenterPrice.yProperty().bind(heightProperty().subtract(100).multiply(0.75).add(50));
		bottomCenterPrice.setFont(Font.font("Courier", 12));
		getChildren().add(bottomCenterPrice);
	}
	
	/**
	 * Draws average horizontal line
	 */
	private void drawAveragePriceLine() {
		Line averageLine = new Line();
		averageLine.setStartX(50);
		averageLine.startYProperty().bind(heightProperty().subtract(100).divide((this.theStockManager.getHighestClosingPrice() 
				- this.theStockManager.getLowestClosingPrice()) * 100).multiply((this.theStockManager.getHighestClosingPrice() - this.theStockManager.getAverageClosingPrice()) * 100).add(50));
		averageLine.endXProperty().bind(widthProperty().subtract(50));
		averageLine.endYProperty().bind(heightProperty().subtract(100).divide((this.theStockManager.getHighestClosingPrice() 
				- this.theStockManager.getLowestClosingPrice()) * 100).multiply((this.theStockManager.getHighestClosingPrice() - this.theStockManager.getAverageClosingPrice()) * 100).add(50));
		averageLine.setStrokeWidth(2);
		averageLine.setStroke(Color.BLUE);
		getChildren().add(averageLine);
	}
	
	/**
	 * Draws First date
	 */
	private void drawFirstDate() {
		Text firstDate = new Text();
		firstDate.setText("" + this.theStockManager.getStockRecord(0).formatHistoricalDate("M/d/yyyy"));
		firstDate.setX(50);
		firstDate.yProperty().bind(heightProperty().subtract(25));
		firstDate.setFont(Font.font("Courier", 15));
		getChildren().add(firstDate);
	}
	
	/**
	 * Draws middle date
	 */
	private void drawMiddleDate() {
		Text middleDate = new Text();
		middleDate.setText("" + this.theStockManager.getStockRecord(this.theStockManager.getStockManagerSize() / 2).formatHistoricalDate("M/d/yyyy"));
		middleDate.xProperty().bind(widthProperty().multiply(0.46));
		middleDate.yProperty().bind(heightProperty().subtract(25));
		middleDate.setFont(Font.font("Courier", 15));
		getChildren().add(middleDate);
	}
	
	/**
	 * Draws Last date
	 */
	private void drawLastDate() {
		Text lastDate = new Text();
		lastDate.setText("" + this.theStockManager.getStockRecord(this.theStockManager.getStockManagerSize() - 1).formatHistoricalDate("M/d/yyyy"));
		lastDate.xProperty().bind(widthProperty().subtract(120));
		lastDate.yProperty().bind(heightProperty().subtract(25));
		lastDate.setFont(Font.font("Courier", 15));
		getChildren().add(lastDate);
	}
	
	/**
	 * Draws first vertical price line
	 */
	private void drawFirstPriceLine() {
		Line firstPriceLine = new Line();
		firstPriceLine.setStartX(50);
		firstPriceLine.startYProperty().bind(heightProperty().subtract(50));
		firstPriceLine.setEndX(50);
		firstPriceLine.setEndY(50);
		firstPriceLine.setStrokeWidth(1);
		firstPriceLine.setStroke(Color.BLACK);
		getChildren().add(firstPriceLine);
	}
	
	/**
	 * Draws middle vertical price line
	 */
	private void drawMiddlePriceLine() {
		Line middlePriceLine = new Line();
		middlePriceLine.startXProperty().bind(widthProperty().multiply(0.5));
		middlePriceLine.startYProperty().bind(heightProperty().subtract(50));
		middlePriceLine.endXProperty().bind(widthProperty().multiply(0.5));
		middlePriceLine.setEndY(50);
		middlePriceLine.setStrokeWidth(1);
		middlePriceLine.setStroke(Color.BLACK);
		getChildren().add(middlePriceLine);
	}
	
	/**
	 * Draws last vertical price line
	 */
	private void drawLastPriceLine() {
		Line lastPriceLine = new Line();
		lastPriceLine.startXProperty().bind(widthProperty().subtract(50));
		lastPriceLine.startYProperty().bind(heightProperty().subtract(50));
		lastPriceLine.endXProperty().bind(widthProperty().subtract(50));
		lastPriceLine.setEndY(50);
		lastPriceLine.setStrokeWidth(1);
		lastPriceLine.setStroke(Color.BLACK);
		getChildren().add(lastPriceLine);
	}
	
	/**
	 * Draws Stock label
	 */
	private void drawStockSymbol() {
		Text stockSymbol = new Text();
		stockSymbol.setText(this.removeFileExtention(this.theStockManager.getFileName()));
		stockSymbol.xProperty().bind(widthProperty().multiply(0.46));
		stockSymbol.setY(40);
		stockSymbol.setFont(Font.font("Courier", 15));
		getChildren().add(stockSymbol);
	}
	
	/**
	 * Draws Stock Price points  
	 */
	private void drawStockPoints() {
		int count = 1;
		for (StockRecord current : this.theStockManager.getAllRecords()) {
			Circle stockPoint = new Circle();
			stockPoint.centerXProperty().bind(widthProperty().subtract(50).subtract(widthProperty().subtract(100).multiply(this.theStockManager.getStockManagerSize() 
					- count).divide(this.theStockManager.getStockManagerSize() - 1)));
			stockPoint.centerYProperty().bind(heightProperty().subtract(100).divide((this.theStockManager.getHighestClosingPrice() 
					- this.theStockManager.getLowestClosingPrice()) * 100).multiply((this.theStockManager.getHighestClosingPrice() 
							- current.getClosingPrice()) * 100).add(50));
			stockPoint.setRadius(5);
			stockPoint.setStroke(Color.BLACK);
			stockPoint.setFill(Color.RED);
			count++;
			getChildren().add(stockPoint);
		}
	}
	
	/**
	 * Removes csv extention from fileName
	 */
	private String removeFileExtention(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf('.'));
	}
}
