package edu.westga.cs6312.stock.view;

import java.util.Scanner;

import edu.westga.cs6312.stock.model.StockManager;

/**
 * Creates user interface to interact with the StockRecord and StockManager class 
 * @author JEANNIE BLAKELY
 * @version 4-20-18
 */
public class StockTUI {
	private Scanner scan;
	private StockManager myStockManager;
	
	/**
	 * 1 Parameter constructor that accepts a StockManager object and 
	 * initializes instance variables
	 * @precondition	theStockManager != null
	 * @param 			theStockManager StockManager object
	 */
	public StockTUI(StockManager theStockManager) {
		this.scan = new Scanner(System.in);
		try {
			this.myStockManager = theStockManager;
		} catch (NullPointerException npe) {
			System.out.println("Invalid StockManager Object");
		}
	}
	
	/**
	 * Method to run
	 * @param none
	 */
	public void run() {
		this.getDisplay("");
		int choice = 0;
		do {
			this.displayMenu();
			choice = this.getUserInt("Please enter your choice: ");
			switch (choice) {
				case 1:
					this.getDisplay("");
					this.getDisplay(this.myStockManager.getFileName());
					this.getDisplay(this.getSummaryData());
					break;
				case 2:
					this.getDisplay("");
					this.getDisplay(this.myStockManager.getFileName());
					this.getDisplay(this.getStatisticalData());
					break;
				case 3:
					this.getDisplay("");
					this.getDisplay(this.myStockManager.getFileName());
					this.getDisplay(this.getAllRecords());
					break;
				case 4:
					break;
				default:
					this.getDisplay("That was not a valid selection. Please enter an integer from 1 - 4.");
					this.getDisplay("");
					break;
			} System.out.println();
		} while (choice != 4);
		this.getDisplay("Thanks for using the StockManager. Goodbye");
	}
	
	/**
	 * Displays a list of menu items to the console.
	 */
	private void displayMenu() {
		System.out.println("\t 1 - View summary data");
		System.out.println("\t 2 - View statistical data");
		System.out.println("\t 3 - View all records");
		System.out.println("\t 4 - Quit");
	}
	
	/**
	 * Returns a summary of StockRecords (first, middle and last record in StockManager ArrayList)
	 */
	private String getSummaryData() {
		String summaryData = "";
		summaryData = "First Record: " + String.format("%35s", this.myStockManager.getStockRecord(0).toString());
		summaryData += "\nMiddle Record: " + String.format("%34s", this.myStockManager.getStockRecord(this.myStockManager.getAllRecords().size() / 2).toString());
		summaryData += "\nLast Record: " + String.format("%36s", this.myStockManager.getStockRecord(this.myStockManager.getAllRecords().size() - 1));
		return summaryData;
	}
	
	/**
	 * Returns a statistical overview of StockRecords (highest, lowest and average closing prices)
	 */
	private String getStatisticalData() {
		String statisticalData = "";
		statisticalData = "Highest Closing Price: \t" +  "$" + String.format("%.02f", this.myStockManager.getHighestClosingPrice());
		statisticalData += "\nLowest Closing Price: \t" + "$" + String.format("%.02f", this.myStockManager.getLowestClosingPrice());
		statisticalData += "\nAverage Closing Price: \t" + "$" + String.format("%.02f", this.myStockManager.getAverageClosingPrice());
		return statisticalData;
	}
	
	/**
	 * Returns a list of String objects for each StockRecord.
	 */
	private String getAllRecords() {
		String allStockRecords = "";
		if (this.myStockManager.getAllRecords().size() == 0) {
			allStockRecords = "There are no stock records on the list";
		} else {
			allStockRecords = this.myStockManager.toString();
		}
		return allStockRecords;
	}
	
		
	/**
	 * Converts string input into integers
	 *@param 	message instruction to be printed to the user
	 *@return 	int value
	 */
	private int getUserInt(String message) {
		int intInput = 0;
		String input = "";
		boolean isValid = false;
		while (!isValid) {
			try {
				System.out.print(message);
				input = this.scan.nextLine();
				intInput = Integer.parseInt(input);
				isValid = true;
			} catch (NumberFormatException ex) {
				System.out.println(input + " is not a valid entry.  Please enter an integer");
			} 
		} 
		return intInput;
	}
	
	/**
	 * Converts string input into System.out.println display
	 */
	private void getDisplay(String message) {
		System.out.println(message);
	}
}
